import {Component} from '@angular/core';
import {CellModel} from "./cell.model";

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent  {
  tries = 0;
  cells: CellModel[] = [];
  random = 0;
  win = false;

  constructor() {
    this.create();
  }

  create() {
    for (let i = 0; i < 36; i++) {
      const cell = new CellModel();
      this.cells.push(cell);
    }
    this.random = Math.floor(Math.random() * this.cells.length);
    this.cells[this.random].hasItem = true;
  }

  reset() {
    this.tries = 0;
    this.cells.splice(0, this.cells.length);
    this.create();
    this.win = false;
  }

  click(index: number, event: Event) {
    const target = <HTMLDivElement>event.target;

    if (!this.cells[index].click) {
      this.tries++;
      this.cells[index].click = true;
      if (target.innerText === 'o') {
        alert('Вы нашли элемент с ' + this.tries + 'й попытки');
      }
    } else {
      return;
    }

    if (this.cells[index].hasItem) {
      this.win = true;
      return;
    }
  }


  randomItem(index: number) {
    if (this.cells[index].hasItem) {
      return 'o';
    } else {
      return '';
    }
  }

  getClassName() {
    if (this.win) {
      return 'board dis';
    } else {
      return 'board';
    }
  }


}
