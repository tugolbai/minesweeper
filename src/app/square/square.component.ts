import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.css']
})
export class SquareComponent  {
  @Input() o = '';
  className = 'black';

  getSquareClass() {
    return 'square ' + this.className;
  }

  onClick() {
    this.className = 'white';
  }


}
